package com.onetoone.demo;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.onetoone.demo.model.Instructor;
import com.onetoone.demo.repository.InstructorRepository;


@RunWith(SpringRunner.class)
@DataJpaTest
class InstructorControllerTest {

	@Autowired
	private InstructorRepository instructorRepository;
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	void testGetInstructors() {
		Instructor instructor1 = new Instructor("Philippe","Luminet","plumi@gmail.com");
		Instructor instructor2 = new Instructor("Jean","Galfione","gigi@gmail.com");
		
		entityManager.persist(instructor1);
		entityManager.persist(instructor2);
		
		List<Instructor> instructors = instructorRepository.findAll();
		assertThat(instructors.size()).isEqualTo(2);
		
		
	}

	@Test
	void testGetInstructorById() {
		Instructor instructor1 = new Instructor("Philippe","Luminet","plumi@gmail.com");
		Instructor instructorSavedInBd = entityManager.persist(instructor1);
		Instructor instructorFromDb = instructorRepository.getOne(instructorSavedInBd.getId());
		assertEquals(instructorSavedInBd, instructorFromDb);
		assertThat(instructorFromDb.equals(instructorSavedInBd));
	}

	@Test
	void testCreateInstructor() {
		Instructor instructor1 = new Instructor("Philippe","Luminet","plumi@gmail.com");
		Instructor instructorSavedInBd = entityManager.persist(instructor1);
		Instructor instructorFromDb = instructorRepository.getOne(instructorSavedInBd.getId());
		assertEquals(instructorSavedInBd, instructorFromDb);
		assertThat(instructorFromDb.equals(instructorSavedInBd));
	}

	@Test
	void testUpdateInstructor() {
		Instructor instructor1 = new Instructor("Philippe","Luminet","plumi@gmail.com");
		Instructor instructorSavedInBd = entityManager.persist(instructor1);
		Instructor instructorFromDb = instructorRepository.getOne(instructorSavedInBd.getId());
		instructorFromDb.setLastName("Lumineti");
		entityManager.persist(instructorFromDb);
		Instructor instructorFromDbNew = instructorRepository.getOne(instructorFromDb.getId());
		assertThat(instructorFromDbNew.getLastName()).isEqualTo("Lumineti");
		
	}

	@Test
	void testDeleteInstructor() {
		Instructor instructor1 = new Instructor("Philippe","Luminet","plumi@gmail.com");
		Instructor instructor2 = new Instructor("Jean","Galfione","gigi@gmail.com");
		Instructor instructorSavedInBd = entityManager.persist(instructor1);
		entityManager.persist(instructor2);
		entityManager.remove(instructorSavedInBd);
		List<Instructor> instructors = instructorRepository.findAll();
		assertThat(instructors.size()).isEqualTo(1);
	}

}
