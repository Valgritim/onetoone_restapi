package com.onetoone.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnetooneRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnetooneRestapiApplication.class, args);
	}

}
