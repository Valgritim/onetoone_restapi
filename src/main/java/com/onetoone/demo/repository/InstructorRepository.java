package com.onetoone.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.onetoone.demo.model.Instructor;


public interface InstructorRepository extends JpaRepository<Instructor, Long>{	
	
}
